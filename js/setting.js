//menu header
$('.mobile-icon').on('click', function () {
	$(this).find(".ico-bar").toggleClass("close");
	$(this).toggleClass("mobile-close");
	var txtc = $.trim($(this).find('.txt').text());
	console.log(txtc);
	if (txtc == "menu") {
		$(this).find('.txt').text('Close');
	} else {
		$(this).find('.txt').text('menu');
	}
	
	$("html").toggleClass("is-locked");
	// $(".nav-menu").slideToggle();
	$(".nav-menu").fadeToggle();
});
if ($(window).width() <= 640) {
	$('.jssub').siblings('.list-menusub01').slideUp();
	$('.jssub').on('click', function () {
		$(this).siblings('.list-menusub01').slideToggle();
		$(this).toggleClass('jsopen');
	});
} else {
	$('.jssub').siblings('.list-menusub01').slideDown();
}
$(window).resize(function () {
	if ($(window).width() <= 640) {
		$('.jssub').siblings('.list-menusub01').slideUp();
		$('.jssub').on('click', function () {
			$(this).siblings('.list-menusub01').slideToggle();
			$(this).toggleClass('jsopen');
		});
	} else {
		$('.jssub').siblings('.list-menusub01').slideDown();
	}
});


// resize slider load page
var window_type;
var $window = $(window);
if ($window.width() <= 834) {
	window_type = 'sp';
} else {
	window_type = 'pc';
}
$(window).resize(function () {
	if ($window.width() <= 834) {
		if ((window_type != 'sp')) {
			location.reload();
		}
	} else {
		if (window_type != 'pc') {
			location.reload();
		}
	}
});


$(function () {
	objectFitImages('img');
});


//matchHeight
$('.list-news .ttl-news').matchHeight();
$('.academy-list__ttl').matchHeight();
$('.academy-list02__logo').matchHeight();
$('.flow-block .item-wp').matchHeight();
jQuery(function ($) {
	$('.list-news .ttl-news').matchHeight();
	$('.academy-list__ttl').matchHeight();
	$('.academy-list02__logo').matchHeight();
	$('.flow-block .item-wp').matchHeight();
});


//backtop
jQuery(document).ready(function ($) {
	$(".backtop").hide();
	$(window).on("scroll", function () {
		if ($(window).scrollTop() > $("#footer").offset().top - $(window).outerHeight()) {
			$("#backtop").addClass("active");
		} else {
			$("#backtop").removeClass("active");
		}
		if ($(this).scrollTop() > 100) {
			$(".backtop").fadeIn("fast");
		} else {
			$(".backtop").fadeOut("fast");
		}
	});
	$('.backtop').click(function () {
		$('body,html').animate({
			scrollTop: 0
		}, 500);
		return false;
	});
});


var $slider = $('.list-slides');

if ($slider.length) {
	var currentSlide;
	var slidesCount;
	var sliderCounter = document.createElement('div');
	sliderCounter.classList.add('slider__counter');
	
	var updateSliderCounter = function (slick, currentIndex) {
		currentSlide = slick.slickCurrentSlide() + 1;
		slidesCount = slick.slideCount;
		if (slidesCount < 10) {
			$(sliderCounter).text("0" + currentSlide);
		} else {
			$(sliderCounter).text("0" + currentSlide);
		}
		
	};
	
	$slider.on('init', function (event, slick) {
		$slider.append(sliderCounter);
		updateSliderCounter(slick);
	});
	
	$slider.on('afterChange', function (event, slick, currentSlide) {
		updateSliderCounter(slick, currentSlide);
	});
	
	$slider.slick();
}

$('.js-slide').slick({
	dots: false,
	arrows: false,
	infinite: false,
	speed: 300,
	slidesToShow: 3,
	slidesToScroll: 1,
	autoplay: false,
	autoplaySpeed: 2000,
	responsive: [
		{
			breakpoint: 640,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 1,
				infinite: true,
				autoplay: true,
			}
		},
		{
			breakpoint: 480,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
				infinite: true,
				autoplay: true,
			}
		}
	]
});

$('.js-slide04').slick({
	dots: false,
	arrows: false,
	infinite: false,
	speed: 300,
	slidesToShow: 4,
	slidesToScroll: 1,
	autoplay: false,
	autoplaySpeed: 2000,
	responsive: [
		{
			breakpoint: 834,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 1,
				autoplay: true,
				infinite: true,
			}
		},
		{
			breakpoint: 480,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
				autoplay: true,
				infinite: true,
			}
		}
	]
});


$('.list-imgmain').slick({
	slidesToShow: 1,
	slidesToScroll: 1,
	arrows: false,
	fade: true,
	asNavFor: '.list-txtthumb'
});
$('.list-txtthumb').slick({
	slidesToShow: 1,
	slidesToScroll: 1,
	asNavFor: '.list-imgmain',
	dots: false,
	centerPadding: '25px',
	centerMode: true,
	focusOnSelect: true
});


// js link
// direct browser to top right away
if (window.location.hash)
	scroll(0, 0);
// takes care of some browsers issue
setTimeout(function () {
	scroll(0, 0);
}, 1);
$(function () {
//your current click function
	var headerHeight = $("#header").outerHeight();
// if we have anchor on the url (calling from other page)
	if (window.location.hash) {
// smooth scroll to the anchor id
		$('html,body').animate({
			scrollTop: $(window.location.hash).offset().top - headerHeight
		}, 800);
	}
});
$(function () {
	$('.link[href^="#"]').click(function () {
// e.preventDefault();
		var headerHeight = $("#header").outerHeight();
		var speed = 800;
		var href = jQuery(this).attr("href");
		var target = jQuery(href == "#" || href == "" ? 'html' : href);
		var position = target.offset().top - headerHeight;
		$('body,html').animate({scrollTop: position}, speed, 'swing');
		return false;
	});
});


$(document).ready(function(){
    $('ul.list-tabs li').click(function(){
        var tab_id = $(this).attr('data-tab');

        $('ul.list-tabs li').removeClass('current');
        $('.tabs-content').removeClass('current');

        $(this).addClass('current');
        $("#"+tab_id).addClass('current');
    })

})